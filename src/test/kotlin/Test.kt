import com.pajato.ctci.Node
import com.pajato.ctci.createLinkedList
import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `verify empty lists generate an empty list`() {
        assertEquals(null, sumLists(null, null))
    }

    @Test fun `verify one empty list returns the other non-empty list`() {
        val list1: Node? = null
        val list2 = Node(1)
        val expected = Node(1)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify one plus one returns two`() {
        val list1 = Node(1)
        val list2 = Node(1)
        val expected = Node(2)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify nine plus nine returns eighteen`() {
        val list1 = Node(9)
        val list2 = Node(9)
        val expected = createLinkedList(8, 1)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify 9 plus 234 returns 18`() {
        val list1 = Node(9)
        val list2 = createLinkedList(4, 3, 2)
        val expected = createLinkedList(3, 4, 2)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }

    @Test fun `verify the CtCI example`() {
        val list1 = createLinkedList(7, 1, 6)
        val list2 = createLinkedList(5, 9, 2)
        val expected = createLinkedList(2, 1, 9)
        assertEquals(expected, sumLists(list1, list2))
        assertEquals(expected, sumLists(list2, list1))
    }
}
