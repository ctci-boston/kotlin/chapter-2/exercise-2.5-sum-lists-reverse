import com.pajato.ctci.Node

fun sumLists(list1: Node?, list2: Node?): Node? {
    tailrec fun sumLists(carry: Int, head: Node, current: Node, left: Node?, right: Node?): Node {
        val sum = carry + (left?.value ?: 0) + (right?.value ?: 0)
        val nextCarry = if (sum >= 10) 1 else 0

        current.value = if (sum >= 10) sum - 10 else sum
        if (nextCarry == 0 && left?.next == null && right?.next == null) return head
        val nextResult = Node(0)
        current.next = nextResult
        return sumLists(nextCarry, head, nextResult, left?.next, right?.next)
    }

    val head = Node(0)
    return if (list1 == null && list2 == null) null else sumLists(0, head, head, list1, list2)
}
